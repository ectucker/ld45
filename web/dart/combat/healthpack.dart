part of ld45;

class Healthpack implements Entity {

  World world;

  Texture texture;
  Vector4 color = Colors.white;

  Vector4 baseColor;
  double time = 0;

  bool remove = false;

  double x;
  double y;

  int charges = 3;

  int healing;

  Healthpack(this.world, this.healing, this.texture, this.x, this.y) {
    baseColor = color;
  }

  void update(double delta) {
    time += delta;
    color = baseColor * (0.9 + 0.1 * cos(time * 4));
  }

  Vector2 get center => Vector2(x + 5, y + 5);

}