part of ld45;

Random rand = Random();

makeFireball(World world, int team, double centerX, double centerY, Vector2 direction) {
  world.game.assetManager.get("fireball_cast").play();
  Projectile proj = Projectile(world, centerX - 15, centerY - 15, FIREBALL_HITBOX);
  proj.velocity = direction * FIREBALL_SPEED;
  proj.texture = world.atlas['fireball'];
  proj.time = FIREBALL_TIME;
  proj.team = team;
  proj.color = FIREBALL_COLOR;
  proj.trailDuration = 0.5;
  proj.onEnd = (Projectile proj, Character hit) {
    for(Entity e in proj.world.entities) {
      if(e is Character) {
        double dist = e.center.distanceTo(proj.center);
        if(e.team != proj.team && dist <= FIREBALL_RADIUS) {
          e.health -= FIREBALL_DAMAGE;
          if(e is Enemy) {
            e.pushVelocity = (e.center - proj.center).normalized() * KNOCKBACK_SPEED;
            e.pushTime = KNOCKBACK_TIME;
          }
        }
      }
    }
    world.game.assetManager.get("fireball_explosion").play();
    makeExplosion(world, FIREBALL_COLOR, proj.centerX, proj.centerY,
        FIREBALL_EXPLOSION_TIME, 20);
  };
  world.add(proj);
}

makeFrost(World world, int team, double centerX, double centerY, Vector2 direction) {
  world.game.assetManager.get("frost_cast").play();
  double dirAngle = atan2(direction.x, direction.y);
  for(int i = 0; i < FROST_PARTICLES; i++) {
    Projectile proj = Projectile(world, centerX - 5, centerY - 5,  FROST_HITBOX);
    double angle =
        (dirAngle - FROST_ANGLE / 2) + i * (FROST_ANGLE / FROST_PARTICLES);
    Vector2 direction = Vector2(sin(angle), cos(angle));
    direction *= FROST_SPEED;
    proj.velocity = direction;
    proj.texture = world.atlas['frost'];
    proj.time = FROST_TIME;
    proj.team = team;
    proj.color = FROST_COLOR;
    proj.onEnd = (Projectile proj, Character hit) {
      world.game.assetManager.get("frost_hit").playIfNot();
      if(hit != null) {
        hit.health -= FROST_DAMAGE;
        if(hit is Enemy) {
          hit.pushVelocity = (hit.center - proj.center).normalized() * KNOCKBACK_SPEED;
          hit.pushTime = KNOCKBACK_TIME;
        }
      }
    };
    //world.game.assetManager.get("ice").play();
    world.add(proj);
  }
}

makePlasma(World world, int team, double centerX, double centerY, Vector2 direction) {
  Projectile proj = Projectile(world, centerX - 15, centerY - 15, PLASMA_HITBOX);
  proj.velocity = direction * PLASMA_SPEED;
  proj.texture = world.atlas['plasma'];
  proj.time = PLASMA_TIME;
  proj.team = team;
  proj.color = PLASMA_COLOR;
  proj.trailDuration = 0.8;
  proj.extraMove = (Projectile proj, double delta) {
    world.game.assetManager.get("plasma_wobble").playIfNot();
    Vector2 perp = Vector2(-proj.velocity.x, proj.velocity.y);
    perp.normalize();
    Vector2 shift = perp * PLASMA_WOBBLE_MAG * sin(proj.time * PLASMA_WOBBLE_SPEED) * delta;
    proj.x += shift.x;
    proj.y += shift.y;
  };
  proj.onEnd = (Projectile proj, Character hit) {
    if(hit != null) {
      hit.health -= PLASMA_DAMAGE;
      if(hit is Enemy) {
        hit.pushVelocity = (hit.center - proj.center).normalized() * KNOCKBACK_SPEED;
        hit.pushTime = KNOCKBACK_TIME;
      }
    }
  };
  world.game.assetManager.get("plasma").play();
  world.add(proj);
}

makeExplosion(World world, Vector4 color, double centerX, double centerY,
    double time, int num) {
  for(int i = 0; i < num; i++) {
    Projectile proj = Particle(world, centerX - 5, centerY - 5, [0, 0, 10, 10]);
    proj.texture = world.ascii[rand.nextInt(world.ascii.length)];
    proj.color = color;
    double angle = rand.nextDouble() * 2 * pi;
    Vector2 direction = Vector2(cos(angle), sin(angle));
    proj.velocity = direction * EXPLOSION_SPEED;
    proj.time = time;
    world.add(proj);
  }
}

makeMissile(World world, int team, double centerX, double centerY, Vector2 direction) {
  world.game.assetManager.get("missile_cast").play();
  Projectile proj = Projectile(world, centerX - 5, centerY - 5, MISSILE_HITBOX);
  proj.velocity = direction * MISSILE_SPEED;
  proj.texture = world.atlas['miss'];
  proj.time = MISSILE_TIME;
  proj.team = team;
  proj.color = MISSILE_COLOR;
  proj.trailDuration = 1.0;
  proj.extraMove = (Projectile proj, double delta) {
    world.game.assetManager.get("missile_track").playIfNot();
    Vector2 target;
    if(team == 1) {
      target = proj.world.player.center;
    } else {
      Enemy nearest;
      for (Entity e in proj.world.entities) {
        if(e is Enemy && lineOfSight(world.map, proj.center, e.center)) {
          if(nearest == null || proj.center.distanceTo(e.center) < proj.center.distanceTo(nearest.center)) {
            nearest = e;
          }
        }
      }
      if(nearest != null) {
        target = nearest.center;
      }
    }

    if(target != null) {
      proj.velocity = (target - proj.center).normalized() * MISSILE_SPEED;
    }
  };
  proj.onEnd = (Projectile proj, Character hit) {
    world.game.assetManager.get("missile_hit").play();
    if(hit != null) {
      hit.health -= MISSILE_DAMAGE;
      if(hit is Enemy) {
        hit.pushVelocity = (hit.center - proj.center).normalized() * KNOCKBACK_SPEED;
        hit.pushTime = KNOCKBACK_TIME;
      }
    }
  };
  world.add(proj);
}

makeBeam(World world, int team, double centerX, double centerY, Vector2 direction) {
  Vector2 pos = Vector2(centerX, centerY);
  Vector2 next = pos;
  world.game.assetManager.get("beam_cast").play();
  while(lineOfSight(world.map, pos, next)) {
    Projectile proj = Projectile(world, next.x - 5, next.y - 5,  FROST_HITBOX);
    proj.velocity = Vector2.zero();
    proj.texture = world.atlas['beam'];
    proj.time = BEAM_TIME;
    proj.team = team;
    proj.color = BEAM_COLOR;
    proj.extraMove = (Projectile proj, double delta) {
      proj.remove = false;
    };
    proj.onEnd = (Projectile proj, Character hit) {
      world.game.assetManager.get("beam_fizzle").play();
      if(hit != null) {
        hit.health -= BEAM_DAMAGE;
      }
    };
    world.add(proj);

    next += direction * BEAM_SPACING;
  }
}

const List<int> trail = [39, 44, 46, 96, 249, 250, 252, 253];

makeTrail(World world, double centerX, double centerY, Vector4 color, double duration) {
  Particle proj = Particle(world, centerX - 5, centerY - 5, [0, 0, 10, 10]);
  proj.texture = world.ascii[trail[rand.nextInt(trail.length)]];
  proj.color = color;
  proj.velocity = Vector2.zero();
  proj.time = duration;
  world.add(proj);
}