part of ld45;

class Weapon implements Entity {

  World world;

  Texture texture;
  Vector4 color;

  Vector4 baseColor;
  double time = 0;

  bool remove = false;

  double x;
  double y;

  int charges = 3;

  Function spell;

  Weapon(this.world, this.spell, this.color, this.x, this.y) {
    texture = world.atlas['wand'];
    baseColor = color;
  }

  void update(double delta) {
    time += delta;
    color = baseColor * (0.9 + 0.1 * cos(time * 4));
  }

  Vector2 get center => Vector2(x + 5, y + 5);

}