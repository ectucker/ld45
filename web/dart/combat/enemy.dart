part of ld45;

class Enemy extends Character {

  bool remove = false;

  Texture texture;

  double targetDist = SNOWMAN_FOLLOW_DIST;
  Vector2 playerLastSeen;

  int _health = 0;
  int team = 1;

  double speed;
  double spellTime;
  double spellCooldown = 1.0;

  double colorTime = 0.0;

  Function spell = makeFrost;
  Vector4 spellColor = FROST_COLOR;

  bool playerSeen = false;
  double sightPause = SIGHT_PAUSE_TIME;
  double firePause = FIRE_PAUSE_TIME;

  double wanderTime = 0.0;

  double pushTime = 0.0;
  Vector2 pushVelocity;

  Enemy(world, double x, double y, List<double> hitbox) : super(world, x, y, hitbox);

  update(double delta) {
    targetVelocity.setValues(0, 0);

    spellCooldown -= delta;
    if(playerSeen) {
      sightPause -= delta;
    }
    firePause -= delta;

    if(firePause <= 0) {
      if (lineOfSight(world.map, center, world.player.center)) {
        playerSeen = true;

        if (sightPause <= 0) {
          if ((world.player.center.distanceTo(center) - targetDist).abs() < 15) {
            targetVelocity.setValues(0, 0);
            attack();
          } else if (world.player.center.distanceTo(center) > targetDist) {
            targetVelocity = (world.player.center - center).normalized() * speed;
          } else {
            targetVelocity = -(world.player.center - center).normalized() * speed;
            attack();
          }
        }

        playerLastSeen = world.player.center;
      } else if(playerSeen) {
        wanderTime += delta;
        targetVelocity = Vector2(sin(wanderTime), cos(wanderTime)) * speed;
      }
    }

    pushTime -= delta;
    if(pushTime > 0) {
      targetVelocity += pushVelocity;
    }

    if(health <= 0) {
      remove = true;
    }

    colorTime -= delta;
    if(colorTime < 0 && colorTime > -1) {
      color = Colors.white;
    }

    super.update(delta);
  }

  attack() {
    if(spellCooldown <= 0 && world.player.health > 0) {
      spell(world, 1, centerX, centerY,
          (world.player.center - center).normalized());
      spellCooldown = spellTime;
      firePause = FIRE_PAUSE_TIME;
    }
  }

  get health => _health;
  set health(int health) {
    if(_health > health) {
      world.game.assetManager.get("enemy_hit").play();
    }
    if(_health > 0 && health <= 0) {
      world.game.assetManager.get("enemy_death").play();
      Vector4 deathColor = spellColor * DEATH_COLOR_MULT;
      deathColor.a = 1.0;
      makeExplosion(world, deathColor, centerX, centerY, DEATH_EXPLOSION_TIME, DEATH_EXPLOSION_NUM);
      world.addLow(Weapon(world, spell, spellColor, centerX - 5, centerY - 10));
    }
    _health = health;
    color = HURT_COLOR;
    colorTime = 0.1;
  }

}