part of ld45;

class Fader extends Enemy {

  double fadeCooldown = 0.2;

  Fader(world, double x, double y, List<double> hitbox)
      : super(world, x, y, hitbox);

  @override
  update(double delta) {
    if(lineOfSight(world.map, center, world.player.center)) {
      fadeCooldown -= delta;
      if(fadeCooldown < 0) {
        health -= 2;
        fadeCooldown = 0.5;
      }
    }

    colorTime -= delta;
    if(colorTime < 0 && colorTime > -1) {
      color = Colors.white;
    }

    if(health <= 0) {
      remove = true;
    }
  }

  @override
  set health(int health) {
    if(_health > 0 && health <= 0) {
      makeExplosion(world, FRIEND_COLOR, centerX, centerY, DEATH_EXPLOSION_TIME, DEATH_EXPLOSION_NUM);
      Weapon drop = Weapon(world, spell, spellColor, centerX - 5, centerY - 10);
      drop.charges = 5;
      world.addLow(drop);
    }
    _health = health;
    color = FRIEND_COLOR;
    world.game.assetManager.get("pickup").play();
    colorTime = 0.1;
  }

}