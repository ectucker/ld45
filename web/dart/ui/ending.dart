part of ld45;

const String ENDING_1 = """
##    ##  #######  ##     ##      ###    ########  ######## 
 ##  ##  ##     ## ##     ##     ## ##   ##     ## ##       
  ####   ##     ## ##     ##    ##   ##  ##     ## ##       
   ##    ##     ## ##     ##   ##     ## ########  ######   
   ##    ##     ## ##     ##   ######### ##   ##   ##       
   ##    ##     ## ##     ##   ##     ## ##    ##  ##       
   ##     #######   #######    ##     ## ##     ## ######## 
""";

const String ENDING_2 = """
 ######   #######  ##     ## ########  #######  ##    ## ######## 
##    ## ##     ## ###   ### ##       ##     ## ###   ## ##       
##       ##     ## #### #### ##       ##     ## ####  ## ##       
 ######  ##     ## ## ### ## ######   ##     ## ## ## ## ######   
      ## ##     ## ##     ## ##       ##     ## ##  #### ##       
##    ## ##     ## ##     ## ##       ##     ## ##   ### ##       
 ######   #######  ##     ## ########  #######  ##    ## ######## 
""";

class EndingScreen extends State {

  StateMachine game;

  List<Texture> ascii;

  List<List<int>> targetGrid;
  List<List<int>> grid;

  Camera2D camera;
  SpriteBatch batch;

  bool complete = false;
  double switchTime = 0.1;

  bool switched = false;

  int pixelHeight;
  int pixelWidth;

  Vector4 textColor = Colors.white;

  EndingScreen(this.game);

  @override
  create() {
    ascii = game.assetManager.get("atlas")["font"].split(10, 10, 2, 1);
    batch = SpriteBatch.defaultShader(gl, maxSprites: 10000);
    resize(width, height);
    playTitle(game);
  }

  @override
  render(double delta) {
    batch.projection = camera.combined;
    batch.begin();
    batch.color = textColor;
    for(int y = 0; y < grid.length; y++) {
      if(y == 20) {
        batch.color = Colors.white;
      }
      for(int x = 0; x < grid[y].length; x++) {
        batch.draw(ascii[grid[y][x]], x * TILE_SIZE, y * TILE_SIZE);
      }
    }
    batch.end();
  }

  @override
  resize(int width, int height) {
    double ratio = width / height;

    pixelHeight = WINDOW_HEIGHT;
    pixelWidth = (ratio * pixelHeight).toInt();

    camera = Camera2D.originBottomLeft(pixelWidth, pixelHeight);

    complete = false;

    grid = List.generate(40, (i) =>
        List.generate((pixelWidth / TILE_SIZE).ceil(), (i) => rand.nextInt(169 - 32) + 32));
    targetGrid = List.generate(40, (i) =>
        List.generate((pixelWidth / TILE_SIZE).ceil(), (i) => 32));

    List<String> lines = ENDING_1.split("\n");
    int centerOffset = (grid[0].length - lines[2].length) ~/ 2;
    print(lines[0].length);
    for(int y = 0; y < lines.length; y++) {
      for(int x = 0; x < lines[y].length; x++) {
        if(x + centerOffset < targetGrid[y].length && x + centerOffset > 0) {
          targetGrid[targetGrid.length - 10 - y][x + centerOffset] =
              lines[y].codeUnitAt(x);
        }
      }
    }

    lines = ENDING_2.split("\n");
    centerOffset = (grid[0].length - lines[2].length) ~/ 2;
    for(int y = 0; y < lines.length; y++) {
      for(int x = 0; x < lines[y].length; x++) {
        if(x + centerOffset < targetGrid[y].length && x + centerOffset > 0) {
          targetGrid[targetGrid.length - 22 - y][x + centerOffset] =
              lines[y].codeUnitAt(x);
        }
      }
    }
  }

  @override
  pause() {}

  @override
  resume() {}

  @override
  preload() {}

  @override
  update(double delta) {
    switchTime -= delta;
    if(!complete) {
      if (switchTime <= 0) {
        complete = true;
        switchTime = 0.075;
        for (int y = 0; y < grid.length; y++) {
          for (int x = 0; x < grid[y].length; x++) {
            if (grid[y][x] != targetGrid[y][x]) {
              complete = false;
              grid[y][x]++;
              if (grid[y][x] >= 169) {
                grid[y][x] = 32;
              }
            }
          }
        }

        if(complete) {
          switchTime = 3.0;
        }
      }
    }

    if(complete && switchTime <= 0 && switchTime > -1) {
      Tween()
        ..get = [() => textColor.r, () => textColor.g, () => textColor.b]
        ..set = [(val) => textColor.r = val, (val) => textColor.g = val, (val) => textColor.b = val]
        ..duration = 5.0
        ..target = [FRIEND_COLOR.r, FRIEND_COLOR.g, FRIEND_COLOR.b]
        ..start(tweenManager);
      switchTime = -2.0;
    }
  }

  drawASCII(String text, int x, int y) {
    for(int i = 0; i < text.length; i++) {
      grid[y][x + i] = text.codeUnitAt(i);
    }
  }

}