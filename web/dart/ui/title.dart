part of ld45;

const String TITLE_1 = """
##    ##  #######  ##     ##      ###    ########  ######## 
 ##  ##  ##     ## ##     ##     ## ##   ##     ## ##       
  ####   ##     ## ##     ##    ##   ##  ##     ## ##       
   ##    ##     ## ##     ##   ##     ## ########  ######   
   ##    ##     ## ##     ##   ######### ##   ##   ##       
   ##    ##     ## ##     ##   ##     ## ##    ##  ##       
   ##     #######   #######    ##     ## ##     ## ######## 
""";

const String TITLE_2 = """
##    ##  #######  ######## ##     ## #### ##    ##  ######   
###   ## ##     ##    ##    ##     ##  ##  ###   ## ##    ##  
####  ## ##     ##    ##    ##     ##  ##  ####  ## ##        
## ## ## ##     ##    ##    #########  ##  ## ## ## ##   #### 
##  #### ##     ##    ##    ##     ##  ##  ##  #### ##    ##  
##   ### ##     ##    ##    ##     ##  ##  ##   ### ##    ##  
##    ##  #######     ##    ##     ## #### ##    ##  ######   
""";

class TitleScreen extends State {

  StateMachine game;

  List<Texture> ascii;

  List<List<int>> targetGrid;
  List<List<int>> grid;

  Camera2D camera;
  SpriteBatch batch;

  bool complete = false;
  double switchTime = 0.1;

  bool switched = false;

  int pixelHeight;
  int pixelWidth;

  TitleScreen(this.game);

  @override
  create() {
    ascii = game.assetManager.get("atlas")["font"].split(10, 10, 2, 1);
    batch = SpriteBatch.defaultShader(gl, maxSprites: 10000);
    resize(width, height);
    playTitle(game);
  }

  @override
  render(double delta) {
    batch.projection = camera.combined;
    batch.begin();
    for(int y = 0; y < grid.length; y++) {
      for(int x = 0; x < grid[y].length; x++) {
        batch.draw(ascii[grid[y][x]], x * TILE_SIZE, y * TILE_SIZE);
      }
    }
    batch.end();
  }

  @override
  resize(int width, int height) {
    double ratio = width / height;

    pixelHeight = WINDOW_HEIGHT;
    pixelWidth = (ratio * pixelHeight).toInt();

    camera = Camera2D.originBottomLeft(pixelWidth, pixelHeight);

    complete = false;

    grid = List.generate(40, (i) =>
        List.generate((pixelWidth / TILE_SIZE).ceil(), (i) => rand.nextInt(169 - 32) + 32));
    targetGrid = List.generate(40, (i) =>
        List.generate((pixelWidth / TILE_SIZE).ceil(), (i) => 32));

    List<String> lines = TITLE_1.split("\n");
    int centerOffset = (grid[0].length - lines[2].length) ~/ 2;
    print(lines[0].length);
    for(int y = 0; y < lines.length; y++) {
      for(int x = 0; x < lines[y].length; x++) {
        if(x + centerOffset < targetGrid[y].length && x + centerOffset > 0) {
          targetGrid[targetGrid.length - 10 - y][x + centerOffset] =
              lines[y].codeUnitAt(x);
        }
      }
    }

    lines = TITLE_2.split("\n");
    centerOffset = (grid[0].length - lines[2].length) ~/ 2;
    for(int y = 0; y < lines.length; y++) {
      for(int x = 0; x < lines[y].length; x++) {
        if(x + centerOffset < targetGrid[y].length && x + centerOffset > 0) {
          targetGrid[targetGrid.length - 22 - y][x + centerOffset] =
              lines[y].codeUnitAt(x);
        }
      }
    }
  }

  @override
  pause() {}

  @override
  resume() {}

  @override
  preload() {}

  @override
  update(double delta) {
    switchTime -= delta;
    if(!complete) {
      if (switchTime <= 0) {
        complete = true;
        switchTime = 0.05;
        for (int y = 0; y < grid.length; y++) {
          for (int x = 0; x < grid[y].length; x++) {
            if (grid[y][x] != targetGrid[y][x]) {
              complete = false;
              grid[y][x]++;
              if (grid[y][x] >= 169) {
                grid[y][x] = 32;
              }
            }
          }
        }

        if(complete) {
          switchTime = 1.0;
        }
      }
    }

    if(complete && switchTime <= 0) {
      drawASCII("Press N to Start", (pixelWidth ~/ 20) - 8, 21);
    }

    if(keyboard.keyJustPressed(KeyCode.N) && !switched) {
      game.fadeToState(Game(game, "level1.tmx"));
      switched = true;
    }
  }

  drawASCII(String text, int x, int y) {
    for(int i = 0; i < text.length; i++) {
      grid[y][x + i] = text.codeUnitAt(i);
    }
  }

}

playTitle(BaseGame game) {
  Music title = game.assetManager.get("chill");
  title.volume = 1.0;
  title.loop();
}

playMain(BaseGame game) {
  Music main = game.assetManager.get("main");
  main.volume = 1.0;
  main.loop();
}

fadeMusic(BaseGame game) {
  Music main = game.assetManager.get("main");
  Music title = game.assetManager.get("chill");
  Tween()
    ..get = [() => main.volume, () => title.volume]
    ..set = [(double vol) => main.volume = vol, (double vol) => title.volume = vol]
    ..target = [0.0, 0.0]
    ..duration = 1.9
    ..callback = () {
      main.stop();
      title.stop();
    }
    ..start(game.tweenManager);
}