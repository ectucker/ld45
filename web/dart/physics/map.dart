part of ld45;

bool isSolid(Tilemap map, int x, int y) {
  if(x < 0 || x >= map.width) {
    return true;
  }
  if(y < 0 || y >= map.height) {
    return true;
  }
  for(TileLayer layer in map.layers) {
    Tile tile = layer.getTile(x, y);
    if(tile != null && tile.properties["solid"]) {
      return true;
    }
  }
  return false;
}

bool overlaps(Tilemap map, Hitbox box) {
  int x1 = box.left ~/ TILE_SIZE;
  int x2 = box.right ~/ TILE_SIZE + 1;

  int y1 = box.bottom ~/ TILE_SIZE;
  int y2 = box.top ~/ TILE_SIZE + 1;

  for(int x = x1; x < x2; x++) {
    for(int y = y1; y < y2; y++) {
      if(isSolid(map, x, y)) {
        return true;
      }
    }
  }

  return false;
}

bool lineOfSight(Tilemap map, Vector2 a, Vector2 b) {
  double dist = a.distanceTo(b);
  int segments = dist ~/ 5;
  Vector2 dir = (b - a).normalized();
  for(int i = 0; i < segments; i++) {
    Vector2 check = a + dir * (dist / segments) * i.toDouble();
    if(isSolid(map, check.x ~/ TILE_SIZE, check.y ~/ TILE_SIZE)) {
      return false;
    }
  }
  return true;
}