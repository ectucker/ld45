part of ld45;

abstract class Character extends Hitbox implements Entity, Health {

  World world;
  bool remove;

  Vector2 targetVelocity;
  Vector2 velocity;
  Vector2 lastSigns;

  Texture texture;
  Vector4 color = Colors.white;

  Character(this.world, double x, double y, List<double> hitbox)
      : super(x, y, hitbox) {
    velocity = Vector2.zero();
    targetVelocity = Vector2.zero();
    lastSigns = Vector2.zero();
  }

  void update(double delta) {
    velocity = targetVelocity * ACCELERATION + velocity * (1 - ACCELERATION);

    if(velocity.x != 0) {
      lastSigns.x = velocity.x.sign;
    }
    if(velocity.y != 0) {
      lastSigns.y = velocity.y.sign;
    }

    double remainderX = moveHor(velocity.x * delta);
    double remainderY = moveVert(velocity.y * delta);

    velocity.y += velocity.y.sign * remainderX.abs() / delta * SLIDE_DAMPENING;
    velocity.x += velocity.x.sign * remainderY.abs() / delta * SLIDE_DAMPENING;

    updateBounds();
  }

  double moveHor(double distance) {
    double minHor = minX();
    double maxHor = maxX();

    double initialX = x;

    if (left + distance < minHor) {
      left = minHor + 0.1;
      //velocity.x = 0;
    } else if (right + distance > maxHor) {
      right = maxHor - 0.1;
      //velocity.x = 0;
    } else {
      x += distance;
    }

    return x - (initialX + distance);
  }

  double moveVert(double distance) {
    double minVert = minY();
    double maxVert = maxY();

    double initialY = y;

    if (bottom + distance < minVert) {
      bottom = minVert + 0.1;
      //velocity.y = 0;
    } else if (top + distance > maxVert) {
      top = maxVert - 0.1;
      //velocity.y = 0;
    } else {
      y += distance;
    }

    return y - (initialY + distance);
  }

  bool isSolid(int tileX, int tileY) {
    if (tileX < 0 || tileX >= world.map.width) {
      return true;
    }
    if (tileY < 0 || tileY >= world.map.height) {
      return true;
    }
    for (TileLayer layer in world.map.layers) {
      Tile tile = layer.getTile(tileX, tileY);
      if (tile != null && tile.properties["solid"]) {
        return true;
      }
    }
    return false;
  }

  double minX() {
    int tileX = left ~/ TILE_SIZE;
    int tileY1 = bottom ~/ TILE_SIZE;
    int tileY2 = top ~/ TILE_SIZE;
    for (int x = tileX; x > tileX - SCAN_DIST; x--) {
      for (int y = tileY1; y <= tileY2; y++) {
        if (isSolid(x, y)) {
          return (x + 1) * TILE_SIZE.toDouble();
        }
      }
    }
    return (tileX - SCAN_DIST) * TILE_SIZE.toDouble();
  }

  double maxX() {
    int tileX = right ~/ TILE_SIZE;
    int tileY1 = bottom ~/ TILE_SIZE;
    int tileY2 = top ~/ TILE_SIZE;
    for (int x = tileX; x < tileX + SCAN_DIST; x++) {
      for (int y = tileY1; y <= tileY2; y++) {
        if (isSolid(x, y)) {
          return x * TILE_SIZE.toDouble();
        }
      }
    }
    return (tileX + SCAN_DIST) * TILE_SIZE.toDouble();
  }

  double minY() {
    int tileY = bottom ~/ TILE_SIZE;
    int tileX1 = left ~/ TILE_SIZE;
    int tileX2 = right ~/ TILE_SIZE;
    for (int y = tileY; y > tileY - SCAN_DIST; y--) {
      for (int x = tileX1; x <= tileX2; x++) {
        if (isSolid(x, y)) {
          return (y + 1) * TILE_SIZE.toDouble();
        }
      }
    }
    return (tileY - SCAN_DIST) * TILE_SIZE.toDouble();
  }

  double maxY() {
    int tileY = top ~/ TILE_SIZE;
    int tileX1 = left ~/ TILE_SIZE;
    int tileX2 = right ~/ TILE_SIZE;
    for (int y = tileY; y < tileY + SCAN_DIST; y++) {
      for (int x = tileX1; x <= tileX2; x++) {
        if (isSolid(x, y)) {
          return y * TILE_SIZE.toDouble();
        }
      }
    }
    return (tileY + SCAN_DIST) * TILE_SIZE.toDouble();
  }

}
