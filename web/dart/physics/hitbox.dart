part of ld45;

class Hitbox {

  double x;
  double y;

  double offsetX;
  double offsetY;

  double width;
  double height;

  Vector2 _halfExtents;

  Aabb2 box;

  Texture texture;

  Hitbox(this.x, this.y, List<double> definition) {
    offsetX = definition[0];
    offsetY = definition[1];
    width = definition[2];
    height = definition[3];
    box = Aabb2.minMax(Vector2(x, y), Vector2(x + width, y + height));
    _halfExtents = Vector2(width / 2, height / 2);
  }

  updateBounds() {
    box.setCenterAndHalfExtents(center, _halfExtents);
  }

  double get left => x + offsetX;
  set left(double left) => x = left - offsetX;
  double get right => x + offsetX + width;
  set right(double right) => x = right - offsetX - width;

  double get bottom => y + offsetY;
  set bottom(double bottom) => y = bottom - offsetY;
  double get top => y  + offsetY + height;
  set top(double top) => y = top - offsetY - height;

  Vector2 get center =>
      Vector2(x + offsetX + _halfExtents.x, y + offsetY + _halfExtents.y);
  set center(Vector2 center) {
    x = center.x - offsetX - _halfExtents.x;
    y = center.y - offsetY - _halfExtents.y;
  }

  double get centerX => x + offsetX + _halfExtents.x;
  set centerX(double centerX) => x = center.x - offsetX - _halfExtents.x;
  double get centerY => y + offsetY + _halfExtents.y;
  set centerY(double centerY) => y = center.y - offsetY - _halfExtents.y;

}
