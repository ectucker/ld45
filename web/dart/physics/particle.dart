part of ld45;

class Particle extends Projectile {

  Particle(World world, double x, double y, List<double> hitbox)
      : super(world, x, y, hitbox);

  @override
  update(double delta) {
    time -= delta;
    if(time <= 0 && time > -1) {
      remove = true;
      return onEnd(this, null);
    } else if(overlaps(world.map, this)) {
      remove = true;
      return onEnd(this, null);
    }
    center += velocity * delta;
    extraMove(this, delta);

    updateBounds();
  }

}