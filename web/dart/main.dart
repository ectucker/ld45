library ld45;

import 'dart:async';

import 'package:cobblestone/cobblestone.dart';
import 'dart:html';
import 'dart:math';

part 'game.dart';
part 'physics/character.dart';
part 'consts.dart';
part 'physics/hitbox.dart';
part 'world/projectile.dart';
part 'physics/map.dart';
part 'world/entity.dart';
part 'world/world.dart';
part 'world/player.dart';
part 'combat/spells.dart';
part 'combat/enemy.dart';
part 'combat/health.dart';
part 'combat/weapon.dart';
part 'combat/fader.dart';
part 'physics/particle.dart';
part 'world/exit.dart';
part 'state.dart';
part 'combat/healthpack.dart';
part 'ui/title.dart';
part 'ui/ending.dart';

void main() {
  StateMachine();
}
