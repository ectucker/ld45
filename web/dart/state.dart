part of ld45;

class StateMachine extends BaseGame {

  State state;
  State next;

  double opacity = 0.0;

  Camera2D fullscreen;
  SpriteBatch batch;

  StateMachine() {
    next = TitleScreen(this);
    //next = Game(this, "level6.tmx");
    next.preload();
  }

  switchState(State next) {
    this.state = null;
    this.next = next;
    next.preload();
  }

  fadeToState(State next) {
    fadeMusic(game);
    Tween()
      ..get = [() => opacity]
      ..set = [(double val) => opacity = val]
      ..target = [1.0]
      ..duration = 2.0
      ..callback = () {
        switchState(next);
        Tween()
          ..get = [() => opacity]
          ..set = [(double val) => opacity = val]
          ..target = [0.0]
          ..duration = 2.0
          ..delay = 2.0
          ..start(tweenManager);
      }
      ..start(tweenManager);
    print("fading");
  }

  @override
  create() {
    querySelector("#loading").hidden = true;

    if(assetManager.allLoaded() && next != null) {
      state = next;
      state.create();
      next = null;
    }

    batch = SpriteBatch.defaultShader(gl);
    resize(width, height);
  }

  @override
  render(double delta) {
    state?.render(delta);

    batch.projection = fullscreen.combined;
    batch.begin();
    batch.color = Vector4(1.0, 1.0, 1.0, opacity);
    batch.draw(assetManager.get("atlas")["black"],
        0, 0, width: width, height: height);
    batch.end();
  }

  @override
  update(double delta) {
    if(assetManager.allLoaded() && next != null) {
      state = next;
      state.create();
      state.resize(width, height);
      next = null;
    }
    state?.update(delta);
  }

  @override
  resize(int width, int height) {
    state?.resize(width, height);

    fullscreen = Camera2D.originBottomLeft(width, height);
  }

  @override
  preload() {
    assetManager.load("atlasTex", loadTexture(gl, "img/atlas.png", nearest));
    assetManager.load("atlas",
        loadAtlas("img/atlas.atlas", assetManager.getLoading("atlasTex")));
    assetManager.load("tileset", loadTileset("map/ASCII.tsx",
        atlasLater(assetManager.getLoading("atlas"), "font"), 2, 1));

    assetManager.load("beam_cast", loadSound(audio, "snd/Beam_Cast.ogg"));
    assetManager.load("beam_fizzle", loadSound(audio, "snd/Beam_Fizzle.ogg"));

    assetManager.load("enemy_death", loadSound(audio, "snd/Enemy_Death.ogg"));

    assetManager.load("fireball_cast", loadSound(audio, "snd/Fireball_Cast.ogg"));
    assetManager.load("fireball_explosion", loadSound(audio, "snd/fireball_hit.wav"));

    assetManager.load("frost_cast", loadSound(audio, "snd/Frost_Cast.ogg"));
    assetManager.load("frost_hit", loadSound(audio, "snd/Frost_Hit.ogg"));

    assetManager.load("healthpack", loadSound(audio, "snd/Healthpack.ogg"));
    assetManager.load("pickup", loadSound(audio, "snd/Spell_Pickup.ogg"));

    assetManager.load("missile_cast", loadSound(audio, "snd/Magic_Missle_Cast.ogg"));
    assetManager.load("missile_hit", loadSound(audio, "snd/Magic_Missle_Hit.ogg"));
    assetManager.load("missile_track", loadSound(audio, "snd/Magic_Missle_Tracking.ogg"));

    assetManager.load("plasma_wobble", loadSound(audio, "snd/Plasma_Wobble.ogg"));

    assetManager.load("roll", loadSound(audio, "snd/Roll.ogg"));
    
    assetManager.load("enemy_hit", loadSound(audio, "snd/enemy_hit.wav"));
    assetManager.load("player_hit", loadSound(audio, "snd/player_hit.wav"));
    assetManager.load("plasma", loadSound(audio, "snd/plasma.wav"));

    assetManager.load("chill", loadMusic(audio, "snd/Bones.ogg"));
    assetManager.load("main", loadMusic(audio, "snd/Main.ogg"));
  }

  Future<Texture> atlasLater(
      FutureOr<Map<String, Texture>> atlas, String name) async {
    return (await atlas)[name];
  }

  @override
  config() {
    scaleMode = ScaleMode.resize;
  }

}