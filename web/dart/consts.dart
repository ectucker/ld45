part of ld45;

Vector4 hexColor(String hex) {
  Vector4 color = Vector4.zero();
  Colors.fromHexString(hex, color);
  return color;
}

// Graphics constants
const int TILE_SIZE = 10;

const int TILES_HIGH = 40;
const int WINDOW_HEIGHT = TILES_HIGH * TILE_SIZE;

// Physics constants
const int SCAN_DIST = 3;

const double ACCELERATION = 0.8;
const double SLIDE_DAMPENING = 0.6;

// Character constants
const List<double> PLAYER_HITBOX = [1, 1, 28, 28];
const double PLAYER_SPEED = 100.0;
const int PLAYER_HEALTH = 150;
const double PLAYER_ROLL_TIME = 0.35;
const double PLAYER_ROLL_SPEED = 175.0;

const List<double> SNOWMAN_HITBOX = [1, 1, 28, 28];
const double SNOWMAN_SPEED = 80;
const int SNOWMAN_HEALTH = 40;
const double SNOWMAN_FOLLOW_DIST = 60;
const double SNOWMAN_SPELL_TIME = 1.5;

const List<double> FIRE_HITBOX = [1, 1, 28, 28];
const double FIRE_SPEED = 90;
const int FIRE_HEALTH = 35;
const double FIRE_FOLLOW_DIST = 70;
const double FIRE_SPELL_TIME = 2.0;

const List<double> PLASMABOY_HITBOX = [1, 1, 28, 28];
const double PLASMABOY_SPEED = 100;
const int PLASMABOY_HEALTH = 45;
const double PLASMABOY_FOLLOW_DIST = 75;
const double PLASMABOY_SPELL_TIME = 1.5;

const List<double> LAUNCHER_HITBOX = [1, 1, 28, 28];
const double LAUNCHER_SPEED = 100;
const int LAUNCHER_HEALTH = 35;
const double LAUNCHER_FOLLOW_DIST = 75;
const double LAUNCHER_SPELL_TIME = 1.5;

const List<double> BEAMMAN_HITBOX = [1, 1, 28, 28];
const double BEAMMAN_SPEED = 100;
const int BEAMMAN_HEALTH = 30;
const double BEAMMAN_FOLLOW_DIST = 75;
const double BEAMMAN_SPELL_TIME = 2;

const List<double> FADER_HITBOX = [1, 1, 28, 28];
const int FADER_HEALTH = 8;

const double SIGHT_PAUSE_TIME = 0.3;
const double FIRE_PAUSE_TIME = 0.4;

// Spell constants
const double FIREBALL_SPEED = 100.0;
const double FIREBALL_TIME = 1.0;
const double FIREBALL_RADIUS = 50;
const double FIREBALL_EXPLOSION_TIME = 0.3;
const int FIREBALL_DAMAGE = 25;
const List<double> FIREBALL_HITBOX = [1, 1, 28, 28];
final Vector4 FIREBALL_COLOR = hexColor("#FF6A00");

const double FROST_SPEED = 140.0;
const double FROST_TIME = 2.0;
const double FROST_ANGLE = pi / 3;
const double FROST_PARTICLES = 7;
const int FROST_DAMAGE = 7;
const List<double> FROST_HITBOX = [1, 1, 8, 8];
final Vector4 FROST_COLOR = hexColor("#28E7C8");

const double PLASMA_SPEED = 65.0;
const double PLASMA_TIME = 3.0;
const int PLASMA_DAMAGE = 40;
const double PLASMA_WOBBLE_SPEED = 2500;
const double PLASMA_WOBBLE_MAG = 100;
const List<double> PLASMA_HITBOX = [2, 2, 26, 26];
final Vector4 PLASMA_COLOR = hexColor("#B200FF");

const double MISSILE_SPEED = 95.0;
const double MISSILE_TIME = 3.0;
const int MISSILE_DAMAGE = 15;
const List<double> MISSILE_HITBOX = [1, 1, 8, 8];
final Vector4 MISSILE_COLOR = hexColor("#28E022");

const double BEAM_SPACING = 10.0;
const double BEAM_TIME = 0.4;
const int BEAM_DAMAGE = 5;
final Vector4 BEAM_COLOR = hexColor("#FFF7D5");

const double EXPLOSION_SPEED = 150.0;

const int BIG_HEALTHPACK = 40;
const int SMALL_HEALTHPACK = 20;

const double KNOCKBACK_SPEED = 200.0;
const double KNOCKBACK_TIME = 0.15;

const double DEATH_EXPLOSION_TIME = 0.2;
const int DEATH_EXPLOSION_NUM = 20;

final Vector4 FRIEND_COLOR = hexColor("#E022BB");
final Vector4 HURT_COLOR = hexColor("#D82222");
final double DEATH_COLOR_MULT = 0.6;