part of ld45;

bool dev = false;

class Game extends State {
  String levelName;

  StateMachine game;

  World world;
  Map<String, Texture> atlas;

  Camera2D camera;
  Framebuffer worldView;

  Camera2D fullscreen;

  SpriteBatch batch;
  DebugBatch debug;

  bool showDebug = false;

  int pixelWidth;
  int pixelHeight;

  Map<int, Map<int, int>> backgroundVals = {};
  Random rand = Random();

  double shakeTime = 0.0;
  double shakeMagnitude = 0.0;
  double shakeSpeed = 0.0;

  Game(this.game, this.levelName);

  @override
  create() {
    resize(width, height);
    batch = SpriteBatch.defaultShader(gl, maxSprites: 10000);
    debug = DebugBatch.defaultShader(gl);

    atlas = assetManager.get("atlas");
    Tilemap map = assetManager.get(levelName);

    world = World(this, atlas, map);

    if(levelName.length >= 11 && levelName != "level10.tmx") {
      playTitle(game);
    } else {
      playMain(game);
    }
  }

  @override
  update(double delta) {
    world.update(delta);

    if(dev) {
      if (keyboard.keyJustPressed(KeyCode.H)) {
        showDebug = !showDebug;
      }

      if (keyboard.keyJustPressed(KeyCode.ONE)) {
        world.player.weapon = Weapon(world, makeFireball, FIREBALL_COLOR, 0, 0);
      } else if (keyboard.keyJustPressed(KeyCode.TWO)) {
        world.player.weapon = Weapon(world, makeFrost, FIREBALL_COLOR, 0, 0);
      } else if (keyboard.keyJustPressed(KeyCode.THREE)) {
        world.player.weapon = Weapon(world, makePlasma, FIREBALL_COLOR, 0, 0);
      } else if (keyboard.keyJustPressed(KeyCode.FOUR)) {
        world.player.weapon = Weapon(world, makeMissile, MISSILE_COLOR, 0, 0);
      } else if (keyboard.keyJustPressed(KeyCode.FIVE)) {
        world.player.weapon = Weapon(world, makeBeam, BEAM_COLOR, 0, 0);
      }

      if(keyboard.keyJustPressed(KeyCode.N)) {
        if(world.nextLevel != null) {
          game.fadeToState(Game(game.game, world.nextLevel));
          world.ended = true;
        } else {
          game.fadeToState(EndingScreen(game.game));
          world.ended = true;
        }
      }
    }

    camera.transform.x = world.player.center.x;
    camera.transform.y = world.player.center.y;
    if(shakeTime > 0) {
      shakeTime -= delta;
      camera.transform.x += shakeMagnitude * sin(shakeSpeed * shakeTime);
      camera.transform.y += shakeMagnitude * cos(shakeSpeed * shakeTime);
    } else {
      shakeMagnitude = 0.0;
      shakeSpeed = 0.0;
    }
    if(levelName.length >= 11 && levelName != "level10.tmx") {
      camera.transform.x =
          min(camera.transform.x, world.map.width * TILE_SIZE - pixelWidth / 2);
    }
    camera.update();
  }

  @override
  render(double delta) {
    gl.clearScreen(Colors.black);

    worldView.beginCapture();
    gl.clearScreen(Colors.black);
    batch.projection = camera.combined;
    batch.begin();
    batch.color = Colors.white;
    for(TileLayer layer in world.map.layers) {
      if(layer.name.contains("Message")) {
        batch.color = FRIEND_COLOR;
      } else {
        batch.color = Colors.white;
      }
      layer.render(batch, 0, 0, camera);
    }
    batch.color = Colors.white;
    fillASCII();
    for (Entity e in world.entities) {
      if(e.texture != null) {
        batch.color = e.color;
        batch.draw(e.texture, e.x, e.y);
      }
    }
    batch.end();

    if(showDebug) {
      debug.projection = camera.combined;
      debug.begin();
      for (Entity e in world.entities) {
        if (e is Hitbox) {
          debug.drawBox((e as Hitbox).box);
        }
      }
      debug.end();
    }
    worldView.endCapture();

    batch.projection = fullscreen.combined;
    batch.color = Colors.white;
    batch.begin();
    batch.draw(worldView.texture, -pixelWidth / 2, -pixelHeight / 2,
        width: pixelWidth, height: pixelHeight);
    if(levelName.length < 11 || levelName == "level10.tmx") {
      drawUI();
    }
    batch.end();
  }

  fillASCII() {
    int startX = (min(min(camera.view.point0.x, camera.view.point1.x),
        min(camera.view.point2.x, camera.view.point3.x)) / 10).floor() - 1;
    int startY = (min(min(camera.view.point0.y, camera.view.point1.y),
        min(camera.view.point2.y, camera.view.point3.y)) / 10).floor() - 1;

    int endX = (max(max(camera.view.point0.x, camera.view.point1.x),
        max(camera.view.point2.x, camera.view.point3.x)) / 10).ceil();
    int endY = (max(max(camera.view.point0.y, camera.view.point1.y),
        max(camera.view.point2.y, camera.view.point3.y)) / 10).ceil();

    for(int x = startX; x < endX; x++) {
      for(int y = startY; y < endY; y++) {
        if(x < 0 || x >= world.map.width || y < 0 || y >= world.map.height) {
          batch.draw(pickBackground(x, y), x * 10, y * 10);
        }
      }
    }
  }

  List<int> backgroundChars = List.generate(62, (i) {
    if(i < 10) {
      return i + 48;
    } else if(i < 36) {
      return (i - 10) + 65;
    } else {
      return (i - 36) + 97;
    }
  });

  Texture pickBackground(int x, int y) {
    if(backgroundVals.containsKey(x)) {
      if(backgroundVals[x].containsKey(y)) {
        return world.ascii[backgroundVals[x][y]];
      }
      backgroundVals[x][y] = backgroundChars[rand.nextInt(backgroundChars.length)];
    } else {
      backgroundVals[x] = Map<int, int>();
    }
    return pickBackground(x, y);
  }

  drawUI() {
    batch.draw(atlas['black'], -pixelWidth / 2, -pixelHeight / 2,
        width: pixelWidth, height: TILE_SIZE * 5.5);
    for(int i = 0; i < pixelWidth ~/ TILE_SIZE + 1; i++) {
      batch.draw(world.ascii[196],
          -pixelWidth / 2 + i * TILE_SIZE, -pixelHeight / 2 + 5 * TILE_SIZE);
    }

    double healthPercent = world.player.health / PLAYER_HEALTH;
    int sections = (healthPercent * 8).ceil();
    double fullHealthPercent = (sections - 1) / 8;
    double lastHealthPercent = (healthPercent - fullHealthPercent) / (1.0 / 8.0);
    for(int i = 0; i < sections; i++) {
      Texture lastHealth = Texture.clone(atlas['health']);
      lastHealth.setRegion((lastHealth.u * lastHealth.sourceWidth).toInt(),
          (lastHealth.v * lastHealth.sourceHeight).toInt(),
          (lastHealthPercent * 6).floor() * 10, lastHealth.height);
      if(i < 4) {
        if(i != sections - 1) {
          batch.draw(atlas['health'], 10 + i * 60, -pixelHeight / 2 + 30);
        } else {
          batch.draw(lastHealth, 10 + i * 60, -pixelHeight / 2 + 30);
        }
      } else {
        if(i != sections - 1) {
          batch.draw(atlas['health'], 10 + (i - 4) * 60, -pixelHeight / 2 + 20);
        } else {
          batch.draw(lastHealth, 10 + (i - 4) * 60, -pixelHeight / 2 + 20);
        }
      }
    }

    if(healthPercent <= 0) {
      batch.draw(atlas['restart'], 10, -pixelHeight / 2 + 30);
    }

    if(world.player.weapon != null) {
      for (int i = 0; i < world.player.weapon.charges; i++) {
        if(i < 3) {
          batch.draw(atlas['spell'], -60 * (i + 1) - 10, -pixelHeight / 2 + 30);
        } else {
          batch.draw(atlas['spell'], -60 * (i - 3 + 1) - 10, -pixelHeight / 2 + 20);
        }
      }
    }
  }

  shakeScreen(double time, double magnitude, double speed) {
    shakeTime = max(time, shakeTime);
    shakeMagnitude = max(magnitude, shakeMagnitude);
    shakeSpeed = max(speed, shakeSpeed);
  }

  @override
  resize(int width, int height) {
    double ratio = width / height;

    pixelHeight = WINDOW_HEIGHT;
    pixelWidth = (ratio * pixelHeight).toInt();

    camera = Camera2D.originCenter(pixelWidth, pixelHeight);
    fullscreen = Camera2D.originCenter(pixelWidth, pixelHeight);
    worldView = Framebuffer(gl, width, height);
  }
  @override
  preload() {
    if(!assetManager.hasLoaded(levelName)) {
      assetManager.load(levelName,
          loadTilemap("map/" + levelName,
              tileset: assetManager.getLoading("tileset")));
    }
  }

  @override
  resume() {}

  @override
  pause() {}


}
