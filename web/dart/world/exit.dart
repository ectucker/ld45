part of ld45;

class Exit extends Entity {

  World world;

  Aabb2 bounds;

  String nextLevel;

  bool left = false;
  bool remove = false;

  Exit(this.world, this.nextLevel,
      double x, double y, double width, double height) {
    bounds = Aabb2.minMax(Vector2(x, y), Vector2(x + width, y + height));
  }

  void update(double delta) {
    if(!left && world.player.box.intersectsWithAabb2(bounds)) {
      StateMachine sm = world.game.game;
      sm.fadeToState(Game(sm, nextLevel));
      left = true;
    }
  }

}