part of ld45;

class World {

  Game game;

  Map<String, Texture> atlas;
  List<Texture> ascii;

  Tilemap map;
  String nextLevel;

  List<Entity> entities;

  Player player;

  List<Entity> _toRemove;
  List<Entity> _toAdd;
  List<Entity> _toAddLow;

  bool ended = false;

  double endTime = 2.0;

  World(this.game, this.atlas, this.map) {
    entities = [];
    _toRemove = [];
    _toAdd = [];
    _toAddLow = [];

    ascii = map.tileset.basicTiles.values.map((tile) => tile.texture).toList();

    for(MapObject e in map.objectGroups.first.objects) {
      if (e.name == "Player") {
        player = Player(game, this, e.pos.x, e.pos.y, PLAYER_HITBOX);
      } else if (e.name == "Snowman") {
        Enemy enemy = Enemy(this, e.pos.x, e.pos.y, SNOWMAN_HITBOX);
        enemy.texture = atlas['snowman'];
        enemy.spell = makeFrost;
        enemy.spellColor = FROST_COLOR;
        enemy.spellTime = SNOWMAN_SPELL_TIME;
        enemy.targetDist = SNOWMAN_FOLLOW_DIST;
        enemy.speed = SNOWMAN_SPEED;
        enemy.health = SNOWMAN_HEALTH;
        entities.add(enemy);
      } else if (e.name == "Plasma") {
        Enemy enemy = Enemy(this, e.pos.x, e.pos.y, PLASMABOY_HITBOX);
        enemy.texture = atlas['wackguy'];
        enemy.spell = makePlasma;
        enemy.spellColor = PLASMA_COLOR;
        enemy.spellTime = PLASMABOY_SPELL_TIME;
        enemy.targetDist = PLASMABOY_FOLLOW_DIST;
        enemy.speed = PLASMABOY_SPEED;
        enemy.health = PLASMABOY_HEALTH;
        entities.add(enemy);
      } else if (e.name == "Fire") {
        Enemy enemy = Enemy(this, e.pos.x, e.pos.y, FIRE_HITBOX);
        enemy.texture = atlas['smallguy'];
        enemy.spell = makeFireball;
        enemy.spellColor = FIREBALL_COLOR;
        enemy.spellTime = FIRE_SPELL_TIME;
        enemy.targetDist = FIRE_FOLLOW_DIST;
        enemy.speed = FIRE_SPEED;
        enemy.health = FIRE_HEALTH;
        entities.add(enemy);
      }  else if (e.name == "Missile") {
        Enemy enemy = Enemy(this, e.pos.x, e.pos.y, LAUNCHER_HITBOX);
        enemy.texture = atlas['missileman'];
        enemy.spell = makeMissile;
        enemy.spellColor = MISSILE_COLOR;
        enemy.spellTime = LAUNCHER_SPELL_TIME;
        enemy.targetDist = LAUNCHER_FOLLOW_DIST;
        enemy.speed = LAUNCHER_SPEED;
        enemy.health = LAUNCHER_HEALTH;
        entities.add(enemy);
      }  else if (e.name == "Light") {
        Enemy enemy = Enemy(this, e.pos.x, e.pos.y, BEAMMAN_HITBOX);
        enemy.texture = atlas['wobblyguy'];
        enemy.spell = makeBeam;
        enemy.spellColor = BEAM_COLOR;
        enemy.spellTime = BEAMMAN_SPELL_TIME;
        enemy.targetDist = BEAMMAN_FOLLOW_DIST;
        enemy.speed = BEAMMAN_SPEED;
        enemy.health = BEAMMAN_HEALTH;
        entities.add(enemy);
      } else if(e.name == "Fader") {
        Enemy enemy = Fader(this, e.pos.x, e.pos.y, FADER_HITBOX);
        enemy.texture = atlas['gift'];
        if(e.properties.hasProp("Spell")) {
          switch(e.properties["Spell"]) {
            case "Fireball":
              enemy.spell = makeFireball;
              enemy.spellColor = FIREBALL_COLOR;
              break;
            case "Frost":
              enemy.spell = makeFrost;
              enemy.spellColor = FROST_COLOR;
              break;
            case "Plasma":
              enemy.spell = makePlasma;
              enemy.spellColor = PLASMA_COLOR;
              break;
            case "Missile":
              enemy.spell = makeMissile;
              enemy.spellColor = MISSILE_COLOR;
              break;
            case "Light":
              enemy.spell = makeBeam;
              enemy.spellColor = BEAM_COLOR;
              enemy.sightPause = 1.0;
              break;
          }
        }
        enemy.health = FADER_HEALTH;
        entities.add(enemy);
      } else if(e.name == "Exit") {
        MapRect rect = e;
        Exit exit = Exit(this, rect.properties["Next"],
            rect.pos.x, rect.pos.y, rect.width, rect.height);
        entities.add(exit);
      } else if(e.name == "HealthSmall") {
        Healthpack pack = Healthpack(this, SMALL_HEALTHPACK, atlas["healthsmall"],
            e.pos.x, e.pos.y);
        entities.add(pack);
      } else if(e.name == "HealthBig") {
        Healthpack pack = Healthpack(this, BIG_HEALTHPACK, atlas["healthbig"],
            e.pos.x, e.pos.y);
        entities.add(pack);
      }
    }

    entities.add(player);

    if(map.properties.hasProp("Next")) {
      nextLevel = map.properties["Next"];
    }
  }

  update(double delta) {
    for(Entity e in entities) {
      e.update(delta);
      if(e.remove) {
        _toRemove.add(e);
      }
    }

    for(Entity e in _toRemove) {
      entities.remove(e);
    }
    _toRemove.clear();

    for(Entity e in _toAdd) {
      entities.add(e);
    }
    _toAdd.clear();

    for(Entity e in _toAddLow) {
      entities.insert(0, e);
    }
    _toAddLow.clear();

    bool hasEnemies = false;
    bool hasExit = false;
    for(Entity e in entities) {
      if(e is Exit) {
        hasExit = true;
      }
      if(e is Enemy) {
        hasEnemies = true;
      }
    }
    if(!ended) {
      if (!hasEnemies && !hasExit) {
        endTime -= delta;
        if(endTime <= 0) {
          if(nextLevel != null) {
            game.game.fadeToState(Game(game.game, nextLevel));
            ended = true;
          } else {
            game.game.fadeToState(EndingScreen(game.game));
            ended = true;
          }
        }
      }
    }

    if(!ended && game.keyboard.keyJustPressed(KeyCode.R)) {
      game.game.switchState(Game(game.game, game.levelName));
      ended = true;
    }
  }

  add(Entity entity) {
    _toAdd.add(entity);
  }

  addLow(Entity entity) {
    _toAddLow.add(entity);
  }

}