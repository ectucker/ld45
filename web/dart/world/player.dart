part of ld45;

class Player extends Character {

  Game game;

  Keyboard keyboard;
  Mouse mouse;

  int _health = PLAYER_HEALTH;
  int team = 0;

  bool remove = false;

  double colorTime = 0.0;

  double rollTime = 0;

  Weapon weapon;

  Player(this.game, World world, double x, double y, List<double> hitbox)
      : super(world, x, y, hitbox) {
    keyboard = game.keyboard;
    mouse = game.mouse;
    texture = game.atlas['blip'];
  }

  @override
  update(double delta) {
    rollTime -= delta;
    if(rollTime <= 0) {
      texture = world.atlas['blip'];

      if (keyboard.keyPressed(KeyCode.A) == keyboard.keyPressed(KeyCode.D)) {
        targetVelocity.x = 0;
      } else if (keyboard.keyPressed(KeyCode.A)) {
        targetVelocity.x = -1;
      } else if (keyboard.keyPressed(KeyCode.D)) {
        targetVelocity.x = 1;
      }

      if (keyboard.keyPressed(KeyCode.W) == keyboard.keyPressed(KeyCode.S)) {
        targetVelocity.y = 0;
      } else if (keyboard.keyPressed(KeyCode.W)) {
        targetVelocity.y = 1;
      } else if (keyboard.keyPressed(KeyCode.S)) {
        targetVelocity.y = -1;
      }

      targetVelocity.normalize();
      targetVelocity *= PLAYER_SPEED;

      if (keyboard.keyJustPressed(KeyCode.E)) {
        Entity nearestPickup;
        double nearestDistance = 30;
        for (Entity e in world.entities) {
          if(e is Weapon) {
            if (e.center.distanceTo(this.center) < min(nearestDistance, 25)) {
              nearestPickup = e;
              nearestDistance = e.center.distanceTo(this.center);
            }
          }
          if(e is Healthpack) {
            if(e.center.distanceTo(this.center) < min(nearestDistance, 25)) {
              nearestPickup = e;
              nearestDistance = e.center.distanceTo(this.center);
            }
          }
        }
        
        if(nearestPickup != null) {
          if (nearestPickup is Weapon) {
            if (weapon != null) {
              if(weapon.spell == nearestPickup.spell) {
                print("Same spell ${weapon.charges} ${nearestPickup.charges}");
                weapon.charges += nearestPickup.charges;
                if(weapon.charges > 6) {
                  weapon.charges = 6;
                }
              } else {
                weapon.x = centerX - 5;
                weapon.y = centerY - 5;
                weapon.remove = false;
                world.addLow(weapon);
                this.weapon = nearestPickup;
              }
            } else {
              this.weapon = nearestPickup;
            }
            world.game.assetManager.get("pickup").play();
            nearestPickup.remove = true;
          }

          if (nearestPickup is Healthpack) {
            world.game.assetManager.get("healthpack").play();
            nearestPickup.remove = true;
            health += nearestPickup.healing;
            if(health > PLAYER_HEALTH) {
              health = PLAYER_HEALTH;
            }
          }
        }
      }

      if (mouse.leftJustPressed) {
        castSpell();
      }

      if (mouse.rightJustPressed) {
        rollTime = PLAYER_ROLL_TIME;
        targetVelocity = mouse.worldCoord(game.camera) - center;
        targetVelocity.normalize();
        targetVelocity *= PLAYER_ROLL_SPEED;

        velocity = targetVelocity;

        game.shakeScreen(0.1, 2, 15);

        world.game.assetManager.get("roll").play();
        texture = world.atlas['roll'];
      }
    }

    if(health <= 0) {
      remove = true;
    }

    colorTime -= delta;
    if(colorTime < 0 && colorTime > -1) {
      color = Colors.white;
    }

    super.update(delta);
  }

  castSpell() {
    Vector2 target = mouse.worldCoord(game.camera);
    Vector2 direction = target - center;
    direction.normalize();

    if(weapon != null) {
      weapon.spell(world, 0, centerX, centerY, direction);
      weapon.charges--;
      game.shakeScreen(0.1, 2, 50);
      if(weapon.charges <= 0) {
        weapon = null;
      }
    }
  }

  get health => _health;
  set health(int health) {
    if(rollTime <= 0) {
      if(_health > health) {
        world.game.assetManager.get("player_hit").play();
        game.shakeScreen(0.2, 5, 100);
        if(health < 0) {
          makeExplosion(world, Colors.white, centerX, centerY, DEATH_EXPLOSION_TIME * 5, DEATH_EXPLOSION_NUM);
        }
      }
      _health = health;
      color = HURT_COLOR;
      colorTime = 0.1;
    }
  }
}
