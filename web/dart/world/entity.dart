part of ld45;

abstract class Entity {

  World world;
  bool remove;

  double x;
  double y;

  Texture texture;
  Vector4 color = Colors.white;

  void update(double delta);

}