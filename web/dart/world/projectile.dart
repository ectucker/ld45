part of ld45;

class Projectile extends Hitbox implements Entity {

  World world;
  bool remove = false;

  Vector2 velocity;

  Texture texture;
  Vector4 color = Colors.white;

  double time = -1;
  double trailTime = 0.1;
  double trailDuration = 0.2;

  int team = 0;

  Function(Projectile proj, Character hit) onEnd = (p, e) {};
  Function(Projectile proj, double delta) extraMove = (p, d) {};

  Projectile(this.world, double x, double y, List<double> hitbox) : super(x, y, hitbox) {
    velocity = Vector2.zero();
  }

  update(double delta) {
    time -= delta;
    if(time <= 0 && time > -1) {
      remove = true;
      return onEnd(this, null);
    } else if(overlaps(world.map, this)) {
      remove = true;
      return onEnd(this, null);
    } else {
      for(Entity e in world.entities) {
        if(e is Character && e.team != team && e.box.intersectsWithAabb2(this.box)) {
          if(e is Player) {
            if(e.rollTime <= 0) {
              remove = true;
              return onEnd(this, e);
            }
          } else {
            remove = true;
            return onEnd(this, e);
          }
        }
      }
    }
    center += velocity * delta;
    extraMove(this, delta);

    trailTime -= delta;
    if(trailTime < 0) {
      makeTrail(world, centerX, centerY, color, trailDuration);
      trailTime = 0.1;
    }

    updateBounds();
  }

}