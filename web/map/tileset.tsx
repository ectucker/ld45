<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.2" name="tileset" tilewidth="16" tileheight="16" tilecount="1" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <properties>
   <property name="solid" type="bool" value="true"/>
  </properties>
  <image width="16" height="16" source="../img/atlas/solid.png"/>
 </tile>
</tileset>
